package com.example.scaniaappmodule.di

import com.example.scaniaappmodule.application.ScaniaModuleActivityViewModel
import com.squareup.moshi.Moshi
import org.koin.android.ext.koin.androidApplication
import org.koin.androidx.viewmodel.dsl.viewModel
import org.koin.dsl.module

object AppModule {

    val module = module {
        viewModel {
            ScaniaModuleActivityViewModel(application = androidApplication(), moshi = get())
        }
        single { Moshi.Builder().build() }
    }
}