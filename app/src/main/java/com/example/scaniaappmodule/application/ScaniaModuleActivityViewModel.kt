package com.example.scaniaappmodule.application

import android.app.Application
import android.content.ComponentName
import android.content.Context
import android.content.Intent
import android.content.ServiceConnection
import android.net.Uri
import android.os.IBinder
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.MutableLiveData
import com.example.scaniaappmodule.data.SettingsData
import com.example.scaniaapptools.AIDLSettings
import com.squareup.moshi.JsonAdapter
import com.squareup.moshi.Moshi
import timber.log.Timber

class ScaniaModuleActivityViewModel(application: Application, private val moshi: Moshi) :
    AndroidViewModel(application) {

    private val serviceConnection: ServiceConnection
    private var aidlSettings: AIDLSettings? = null
    val settingsLiveData: MutableLiveData<SettingsData> = MutableLiveData()

    private val adapter: JsonAdapter<SettingsData> = moshi.adapter(SettingsData::class.java)

    init {
        serviceConnection = object : ServiceConnection {
            override fun onServiceConnected(p0: ComponentName?, p1: IBinder?) {
                aidlSettings = AIDLSettings.Stub.asInterface(p1)
                Timber.d("onServiceConnected " + aidlSettings?.settings)
                aidlSettings?.apply {
                    val settingsData = adapter.fromJson(settings)
                    settingsLiveData.postValue(settingsData)
                }


            }

            override fun onServiceDisconnected(p0: ComponentName?) {
                Timber.d("onServiceDisconnected")
            }
        }

    }

    fun checkCanOpenDevTools(input: String) {
        aidlSettings?.let { it ->
            if (input == it.secretCode.toString()) {
                val intent = Intent(Intent.ACTION_VIEW)
                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
                val uri = Uri.parse("app://scania.tools")
                intent.data = uri
                getApplication<AppApplication>().startActivity(intent)
            }
        }

    }

    fun connectService() {
        val intent = Intent("com.example.scaniaapptools.CORE_SERVICE")
        intent.setPackage("com.example.scaniaapptools")


        getApplication<AppApplication>().bindService(
            intent,
            serviceConnection,
            Context.BIND_AUTO_CREATE
        )
    }

    fun disconnectService() {
        getApplication<AppApplication>().unbindService(serviceConnection)
    }

    override fun onCleared() {
        super.onCleared()
        getApplication<AppApplication>().unbindService(serviceConnection)
    }
}