package com.example.scaniaappmodule.application.ui

import android.content.ServiceConnection
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.core.widget.doOnTextChanged
import com.example.scaniaappmodule.application.ScaniaModuleActivityViewModel
import com.example.scaniaapptools.AIDLSettings
import com.example.scaniamodule.databinding.ScaniaModuleActivityBinding
import org.koin.androidx.viewmodel.ext.android.viewModel
import timber.log.Timber

class ScaniaModuleActivity : AppCompatActivity() {

    private var aidlSettings: AIDLSettings? = null
    private var serviceConnection: ServiceConnection? = null

    val viewModel by viewModel<ScaniaModuleActivityViewModel>()

    private lateinit var binding: ScaniaModuleActivityBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setUpView()
        setUpData()

    }

    override fun onResume() {
        super.onResume()
        viewModel.connectService()
    }

    override fun onPause() {
        super.onPause()
        viewModel.disconnectService()
    }

    private fun setUpView() {
        binding = ScaniaModuleActivityBinding.inflate(layoutInflater)
        setContentView(binding.root)
        binding.inputPhone.doOnTextChanged { text, start, before, count ->
            Timber.d("doOnTextChanged: $text")

        }
        binding.btnCall.setOnClickListener {
            Timber.d("setOnClickListener: ${binding.inputPhone.text.toString()}")
            viewModel.checkCanOpenDevTools(binding.inputPhone.text.toString())
        }
    }

    private fun setUpData() {
        viewModel.settingsLiveData.observe(this) {
            Timber.d("settingsLiveData: $it")
            binding.btnSettings.isEnabled = it.enablePhoneSettings
        }
    }
}