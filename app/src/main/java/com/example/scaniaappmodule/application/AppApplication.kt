package com.example.scaniaappmodule.application

import android.app.Application
import com.example.scaniaappmodule.di.AppModule
import org.koin.android.ext.koin.androidContext
import org.koin.android.ext.koin.androidLogger
import org.koin.core.context.startKoin
import timber.log.Timber

class AppApplication : Application() {

    override fun onCreate() {
        super.onCreate()
        Timber.plant(Timber.DebugTree())
        startKoin {
            androidLogger()
            androidContext(this@AppApplication)
            modules(AppModule.module)
        }
    }
}