// AIDLSettings.aidl
package com.example.scaniaapptools;

// Declare any non-default types here with import statements

interface AIDLSettings {

    String getSettings();

    int getSecretCode();

}